from django.apps import AppConfig


class StuinfoConfig(AppConfig):
    name = 'Students.apps.stuinfo'
